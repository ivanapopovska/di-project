package diproject.demo;

import diproject.demo.controllers.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {



        ApplicationContext act = SpringApplication.run(DemoApplication.class, args);
        I18nController i18nController = (I18nController) act.getBean("i18nController");
        MyController myController =  (MyController) act.getBean("myController");
        System.out.println(i18nController.sayHello());
        System.out.println(myController.sayHi());
        System.out.println("------ Property");
        PropertyInjectedController propertyInjectedController = (PropertyInjectedController) act.getBean("propertyInjectedController");
        System.out.println(propertyInjectedController.getGreeting());

        System.out.println("--------- Setter");
        SetGreetingServiceController setterInjectedController = (SetGreetingServiceController) act.getBean("setGreetingServiceController");
        System.out.println(setterInjectedController.getGreeting());

        System.out.println("-------- Constructor" );
        ConstructorInjectedController constructorInjectedController = (ConstructorInjectedController) act.getBean("constructorInjectedController");
        System.out.println(constructorInjectedController.getGreeting());

    }

}
