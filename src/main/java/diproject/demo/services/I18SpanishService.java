package diproject.demo.services;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("es")
@Service("i18")
public class I18SpanishService implements GreetingService {
    @Override
    public String sayGreeting() {
        return "Hi in Spanish";
    }
}
