package diproject.demo.services;


import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile({"en", "default"})
@Service("i18")
public class I18NGreetingService implements GreetingService {
    @Override
    public String sayGreeting() {
        return "Hi in English";
    }
}
